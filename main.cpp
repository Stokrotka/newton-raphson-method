///NEWTON-RAPHSON METHOD is used to approximate the root of a function

#include <iostream>
#include <cmath>

using namespace std;


double func1 (double x)
{
    return cos(x)-x;
    //return x;
}

double func2 (double x)
{
    return (-1)*sin(x)-1;
    //return 1;
}

int main()
{
    double Xold;        //first approximation of a root
    double Xnew;        //next approximation of a root
    double tolerance;   //given tolerance
    double error;       //auxiliary variable

    cout << "Hello, please write your first approximate root and tolerance: " << endl;
    cin>>Xold>>tolerance;

    do
    {
        Xnew= Xold-func1(Xold)/func2(Xold);
        error=fabs((Xnew-Xold)/Xnew);
        Xold=Xnew;

    }while(error>tolerance);

    cout<<"Your approximated root of a function is: "<<Xnew;

    return 0;
}
